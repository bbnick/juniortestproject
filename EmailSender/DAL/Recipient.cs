﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.DAL
{
    public class Recipient
    {
        /// <summary>
        /// Recipient id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Email address
        /// </summary>
        public string Address { get; set; }


        ///// <summary>
        ///// Log id
        ///// </summary>
        //public Guid? LogId { get; set; }

        ///// <summary>
        ///// Log
        ///// </summary>
        //public virtual Log Log { get; set; }

        public virtual ICollection<LogRecipient> LogsRecipients { get; set; }

        public Recipient()
        {
            LogsRecipients = new List<LogRecipient>();
        }
    }
}
