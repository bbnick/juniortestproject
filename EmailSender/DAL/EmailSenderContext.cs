﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.DAL
{
    public class EmailSenderContext : DbContext
    {
        public DbSet<Log> Logs { get; set; }
        public DbSet<Recipient> Recipients { get; set; }

        public EmailSenderContext(DbContextOptions<EmailSenderContext> options) : base(options)
        {
            //Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LogRecipient>()
                .HasKey(e => new { e.LogId, e.RecipientId });

            modelBuilder.Entity<LogRecipient>()
                .HasOne(lr => lr.Log)
                .WithMany(l => l.LogsRecipients)
                .HasForeignKey(lr => lr.LogId);

            modelBuilder.Entity<LogRecipient>()
                .HasOne(lr => lr.Recipient)
                .WithMany(r => r.LogsRecipients)
                .HasForeignKey(lr => lr.RecipientId);
        }
    }
}
