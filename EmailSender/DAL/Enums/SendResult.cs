﻿namespace EmailSender.DAL.Enums
{
    /// <summary>
    /// Email sending result 0 - Ok, 1 - Failed
    /// </summary>
    public enum SendResult
    {
        /// <summary>
        /// All right
        /// </summary>
        Ok = 0,

        /// <summary>
        /// Error: not sended
        /// </summary>
        Failed = 1
    }
}
