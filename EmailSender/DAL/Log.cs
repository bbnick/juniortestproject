﻿using EmailSender.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.DAL
{
    public class Log
    {
        /// <summary>
        /// Log id
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// Email subject
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Email body
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Email recipients
        /// </summary>
        public virtual ICollection<LogRecipient> LogsRecipients { get; set; }

        /// <summary>
        /// Date of email creation
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Email sending result
        /// </summary>
        public SendResult Result { get; set; }

        /// <summary>
        /// Message with sending error
        /// </summary>
        public string FailedMessage { get; set; }

        public Log()
        {
            LogsRecipients = new List<LogRecipient>();
        }
    }
}
