﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.DAL
{
    public class LogRecipient
    {
        public Guid LogId { get; set; }
        public virtual Log Log { get; set; }

        public Guid RecipientId { get; set; }
        public virtual Recipient Recipient { get; set; }
    }
}
