﻿using EmailSender.DAL;
using EmailSender.Extensions;
using EmailSender.Models;
using EmailSender.Models.InputModels;
using EmailSender.Models.ViewModels;
using EmailSender.Services;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Repositories.Implementation
{
    public class EmailSenderRepository : IEmailSenderRepository
    {
        private readonly EmailSenderContext _context;
        private readonly IEmailSender _emailSender;

        public EmailSenderRepository(EmailSenderContext context, IEmailSender emailSender)
        {
            _context = context;
            _emailSender = emailSender;
        }

        public async Task<string> GetAllLogsAsync()
        {
            var logs = await _context.Logs.ToListAsync();

            return JsonConvert.SerializeObject(logs.Select(l => l.ToViewModel()).ToList());
        }

        public async Task<LogViewModel> WriteLogsAsync(EmailInputModel emailInput, ResultModel sendResult)
        {
            var newLog = new Log()
            {
                Subject = emailInput.Subject,
                Body = emailInput.Subject,
                CreationDate = DateTime.UtcNow,
                Result = sendResult.Result,
                FailedMessage = sendResult.FailedMessage
            };

            var recipients = emailInput.Recipients.Distinct().Select(a => new Recipient { Address = a }).ToList();

            //find old recipients in database
            var oldRecipients = _context.Recipients
                .Where(cr => recipients.Select(r => r.Address).Contains(cr.Address)).ToList();

            //select new recipient addreses
            var newAddreses = recipients.Select(r => r.Address).Except(oldRecipients.Select(cr => cr.Address).ToList());

            var newRecipients = recipients.Where(r => newAddreses.Contains(r.Address)).ToList();

            if(oldRecipients.Count != 0)
            {
                //attach newLog to old recipients
                oldRecipients.ForEach(cr => cr.AttachLog(newLog));
            }

            if(newRecipients.Count != 0)
            {
                //attach newLog to new recipients
                newRecipients.ForEach(nr => nr.AttachLog(newLog));
            }

            //add log to database
            await _context.Logs.AddAsync(newLog);
            //add new recipients to database
            await _context.Recipients.AddRangeAsync(newRecipients);

            await _context.SaveChangesAsync();

            var result = await _context.Logs.FindAsync(newLog.Id);

            return result.ToViewModel();
        }
    }
}
