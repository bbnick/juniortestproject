﻿using EmailSender.Models;
using EmailSender.Models.InputModels;
using EmailSender.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Repositories
{
    public interface IEmailSenderRepository
    {
        /// <summary>
        /// Write logs to database
        /// </summary>
        /// <param name="emailInput">Email input data</param>
        /// <param name="sendResult">Email sending result</param>
        /// <returns>Written log</returns>
        Task<LogViewModel> WriteLogsAsync(EmailInputModel emailInput, ResultModel sendResult);

        /// <summary>
        /// Get all logs from database
        /// </summary>
        /// <returns>Logs converted to json</returns>
        Task<string> GetAllLogsAsync();
    }
}
