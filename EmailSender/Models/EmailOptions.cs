﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Models
{
    public class EmailOptions
    {
        /// <summary>
        /// Login for SMTP client
        /// </summary>
        public string Login { get; set; }
        /// <summary>
        /// Password for SMTP client
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Outgoing mail server SMTP
        /// </summary>
        public string SmtpMailServer { get; set; }
        /// <summary>
        /// Port
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// Set "true" if need to use SSL
        /// </summary>
        public bool UseSSL { get; set; }

        /// <summary>
        /// Sender email address
        /// </summary>
        public string SenderAddress { get; set; }
    }
}
