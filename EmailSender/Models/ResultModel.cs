﻿using EmailSender.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Models
{
    public class ResultModel
    {
        /// <summary>
        /// Email sending result
        /// </summary>
        public SendResult Result { get; set; }

        /// <summary>
        /// Error message
        /// </summary>
        public string FailedMessage { get; set; }

        public ResultModel(SendResult result, string failedMessage = null)
        {
            Result = result;
            FailedMessage = failedMessage;
        }
    }
}
