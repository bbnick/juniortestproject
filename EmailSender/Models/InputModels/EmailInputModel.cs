﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Models.InputModels
{
    public class EmailInputModel
    {
        /// <summary>
        /// Subject
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// Body
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// Recipient addreses
        /// </summary>
        [Required]
        public List<string> Recipients { get; set; }
    }
}
