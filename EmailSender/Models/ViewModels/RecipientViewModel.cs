﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Models.ViewModels
{
    public class RecipientViewModel
    {
        /// <summary>
        /// Recipient address
        /// </summary>
        public string Address { get; }

        public RecipientViewModel(string address)
        {
            Address = address;
        }
    }
}
