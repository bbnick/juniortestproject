﻿using EmailSender.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Models.ViewModels
{
    public class LogViewModel
    {
        /// <summary>
        /// Subject
        /// </summary>
        public string Subject { get; }

        /// <summary>
        /// Body
        /// </summary>
        public string Body { get; }

        /// <summary>
        /// Email recipients
        /// </summary>
        public List<RecipientViewModel> Recipients { get; }

        /// <summary>
        /// Email creation date
        /// </summary>
        public DateTime CreationDate { get; }

        /// <summary>
        /// Sending result
        /// </summary>
        public SendResult Result { get; }

        /// <summary>
        /// Error message
        /// </summary>
        public string FailedMessage { get; }

        public LogViewModel(string subject, string body, List<RecipientViewModel> recipients,
            DateTime creationDate, SendResult result, string failedMessage)
        {
            Subject = subject;
            Body = body;
            Recipients = recipients;
            CreationDate = creationDate;
            Result = result;
            FailedMessage = failedMessage;
        }
    }
}
