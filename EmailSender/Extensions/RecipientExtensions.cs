﻿using EmailSender.DAL;
using EmailSender.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Extensions
{
    public static class RecipientExtensions
    {
        /// <summary>
        /// Map Recipient to RecipientViewModel
        /// </summary>
        /// <param name="recipient">recipient entity</param>
        /// <returns>View model of recipient</returns>
        public static RecipientViewModel ToViewModel(this Recipient recipient)
        {
            if (recipient == null)
                return null;

            return new RecipientViewModel(recipient.Address);
        }

        /// <summary>
        /// Attach Log entity to Recipient entity
        /// </summary>
        /// <param name="recipient">Recipient entity</param>
        /// <param name="log">Log entity</param>
        public static void AttachLog(this Recipient recipient, Log log)
        {
            recipient.LogsRecipients.Add(new LogRecipient
            {
                Log = log
            });
        }
    }
}
