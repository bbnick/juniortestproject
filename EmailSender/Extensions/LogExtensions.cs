﻿using EmailSender.DAL;
using EmailSender.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Extensions
{
    public static class LogExtensions
    {
        /// <summary>
        /// Map Log to LogViewModel
        /// </summary>
        /// <param name="log">Log entity</param>
        /// <returns>Log view model</returns>
        public static LogViewModel ToViewModel(this Log log)
        {
            if (log == null)
                return null;

            var recipients = log.LogsRecipients.Select(lr => lr.Recipient.ToViewModel()).ToList();

            return new LogViewModel(
                log.Subject, 
                log.Body, 
                recipients, 
                log.CreationDate, 
                log.Result, 
                log.FailedMessage
                );
        }
    }
}
