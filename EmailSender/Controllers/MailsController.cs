﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmailSender.Models.InputModels;
using EmailSender.Models.ViewModels;
using EmailSender.Repositories;
using EmailSender.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EmailSender.Controllers
{
    [Route("api/mails")]
    [ApiController]
    public class MailsController : ControllerBase
    {
        private readonly IEmailSender _emailSender;
        private readonly IEmailSenderRepository _emailSenderRepo;

        public MailsController(IEmailSender emailSender, IEmailSenderRepository emailSenderRepo)
        {
            _emailSender = emailSender;
            _emailSenderRepo = emailSenderRepo;
        }

        /// <summary>
        /// Get all logs
        /// </summary>
        /// <returns>All logs from database in json format</returns>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var result = await _emailSenderRepo.GetAllLogsAsync();
            return Content(result, "application/json");
        }

        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="email">Email input data</param>
        /// <returns>Email sended log</returns>
        [HttpPost]
        public async Task<LogViewModel> Post([FromBody]EmailInputModel email)
        {
            //send email
            var sendResult = await _emailSender.SendEmailAsync(email);

            //write log to database
            var writeResult = await _emailSenderRepo.WriteLogsAsync(email, sendResult);

            return writeResult;
        }
    }
}
