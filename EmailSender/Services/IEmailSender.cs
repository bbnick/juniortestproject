﻿using EmailSender.DAL.Enums;
using EmailSender.Models;
using EmailSender.Models.InputModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Services
{
    public interface IEmailSender
    {
        /// <summary>
        /// Send email
        /// </summary>
        /// <param name="emailInput">Email input data</param>
        /// <returns>Sending result</returns>
        Task<ResultModel> SendEmailAsync(EmailInputModel emailInput);
    }
}
