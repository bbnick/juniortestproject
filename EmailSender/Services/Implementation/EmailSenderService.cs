﻿using EmailSender.DAL.Enums;
using EmailSender.Models;
using EmailSender.Models.InputModels;
using MailKit.Net.Smtp;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EmailSender.Services.Implementation
{
    public class EmailSenderService : IEmailSender
    {
        private readonly EmailOptions _emailOptions;

        public EmailSenderService(IOptions<EmailOptions> options)
        {
            _emailOptions = options.Value;
        }

        public async Task<ResultModel> SendEmailAsync(EmailInputModel emailInput)
        {
            var email = new MimeMessage();

            email.From.Add(new MailboxAddress(_emailOptions.SenderAddress));

            email.To.AddRange(emailInput.Recipients.Distinct().Select(r => new MailboxAddress(r)).ToList());
            email.Subject = emailInput.Subject;
            email.Body = new TextPart(TextFormat.Text)
            {
                Text = emailInput.Body
            };

            try
            {
                using (var client = new SmtpClient())
                {
                    await client.ConnectAsync(_emailOptions.SmtpMailServer, _emailOptions.Port, _emailOptions.UseSSL);
                    await client.AuthenticateAsync(_emailOptions.Login, _emailOptions.Password);

                    await client.SendAsync(email);

                    await client.DisconnectAsync(true);
                }
            }
            catch (Exception e)
            {
                return new ResultModel(SendResult.Failed, e.Message);
            }

            return new ResultModel(SendResult.Ok);
        }
    }
}
